#!/sw/bin/python2.4
# coding=utf-8
# -*- encoding: utf-8 -*-

import xmlrpclib
import sys
import getopt

class update_partner:
    
    def __init__(self,server_ip,port,dbname,username,pwd):
        # Get the uid
        sock_common = xmlrpclib.ServerProxy ('http://'+server_ip+':'+port+'/xmlrpc/common')
        self.uid = sock_common.login(dbname, username, pwd)
        self.dbname=dbname
        self.pwd=pwd
        self.sock= xmlrpclib.ServerProxy('http://'+server_ip+':'+port+'/xmlrpc/object')
         
    
    def do_update_partner(self):
        count = 0
        inv_list = []
        inv_ids = self.sock.execute(self.dbname, self.uid, self.pwd, 'account.invoice', 'search', [('move_id', '!=', False), ('type', '=', 'out_invoice')])
        if inv_ids:
            for inv_id in inv_ids:
                invoice_vals = self.sock.execute(self.dbname, self.uid, self.pwd, 
                                        'account.invoice', 'read', inv_id, ['tax_line', 'move_id'])
                if 'tax_line' in invoice_vals and invoice_vals['tax_line']:
                    for tax_line_id in invoice_vals['tax_line']:
                        line_dict = self.sock.execute(self.dbname, self.uid, self.pwd, 
                                        'account.invoice.tax', 'read', tax_line_id, ['name'])
                        name = line_dict['name']
                        account_tax_ids = self.sock.execute(self.dbname, self.uid, self.pwd, 'account.tax', 'search', [('name', '=', name)])
                        if account_tax_ids:
                            for account_tax_id in account_tax_ids:
                                account_tax_dict = self.sock.execute(self.dbname, self.uid, self.pwd, 
                                                          'account.tax', 'read', account_tax_id, ['partner_id'])
                                if 'partner_id' in account_tax_dict and account_tax_dict['partner_id']:
                                    partner_id = account_tax_dict['partner_id'][0]
                                    if 'move_id' in invoice_vals and invoice_vals['move_id']:
                                        move_dict = self.sock.execute(self.dbname, self.uid, self.pwd, 
                                                            'account.move', 'read', invoice_vals['move_id'][0], ['line_id'])
                                        if 'line_id' in move_dict and move_dict['line_id']:
                                            for move_line_id in move_dict['line_id']:
                                                move_line_vals = self.sock.execute(self.dbname, self.uid, self.pwd, 
                                                                             'account.move.line', 'read', move_line_id, ['name', 'partner_id'])
                                                if 'name' in move_line_vals and move_line_vals['name'] == name:
                                                    self.sock.execute(self.dbname, self.uid, self.pwd, 'account.move.line', 'write', 
                                                                      [move_line_id], {'partner_id': partner_id})
                                                    inv_list.append(inv_id)
                                                    count += 1
                                                    print 'updating......',count
                                                    
            print 'Total count....invoice ids...',count,list(set(inv_list))
            
          
        
__doc__ ="usage :python partner_updation.py server port database username password"


    
def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
    if len(args) < 5:
        print len(args),args
        print __doc__
    else:
        server_ip = args[0]
        port = args[1]
        database = args[2]
        username = args[3]
        password = args[4]

        ip = update_partner(server_ip,port,database,username,password)
        ip.do_update_partner()
if __name__ == "__main__":
    main()